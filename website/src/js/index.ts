import { getSearchResults } from './api'

function sleep(time: number) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

class Table {
  m_indexRow: number;
  m_indexCol: number;
  m_currHighEl: [number, number];
  m_headerSize: number;
  m_main: HTMLTableElement;

  constructor(headerList: string[]) {
    this.m_indexCol = 0;
    this.m_indexRow = 0;
    this.m_headerSize = headerList.length;
    this.m_currHighEl = [-1, -1];
    this.m_main = this.createTable(headerList);
  }

  protected createTable = (headerList: string[]): HTMLTableElement => {
    const tableBox = document.createElement('div');
    tableBox.setAttribute('id', 'table-box');
    tableBox.setAttribute('name', 'table-box');
    tableBox.setAttribute('align', 'center');
    document.getElementById('dispbox')?.appendChild(tableBox);
    const table = document.createElement('table');
    table.setAttribute('id', 'table')
    table.setAttribute('name', 'table');
    table.setAttribute('class', 'table')
    const tableHead = document.createElement('thead');
    tableHead.setAttribute('id', table.getAttribute('id') + '-head');
    tableHead.setAttribute('name', table.getAttribute('name') + '-head');
    for (let index = 0; index < headerList.length; index++) {
      const tableHeadEl = document.createElement('th');
      tableHeadEl.setAttribute('id', tableHead.getAttribute('id') + '-' + index.toString());
      tableHeadEl.setAttribute('name', tableHead.getAttribute('name') + '-' + index.toString());
      tableHeadEl.innerHTML = headerList[index];
      tableHead.appendChild(tableHeadEl);
    }
    table.appendChild(tableHead);
    tableBox.appendChild(table);
    const rowEl = document.createElement('tr');
    rowEl.setAttribute('id', this.getRowId(table, this.m_indexRow));
    for (let index = 0; index < this.m_headerSize; index++) {
      const el = document.createElement('td');
      el.setAttribute('id', this.getElId(table, this.m_indexRow, index));
      rowEl.appendChild(el);
    }
    table.appendChild(rowEl)
    return table;
  }

  protected getRowId = (table: HTMLTableElement, indexRow: number): string => {
    return table.getAttribute('id') + '-' + indexRow.toString();
  }

  protected getElId = (table: HTMLTableElement, indexRow: number, indexCol: number): string => {
    return this.getRowId(table, indexRow) + '-' + indexCol.toString();
  }

  public pushRow = (table: HTMLTableElement): void => {
    this.m_indexRow++;
    const rowEl = document.createElement('tr');
    rowEl.setAttribute('id', this.getRowId(table, this.m_indexRow));
    for (let index = 0; index < this.m_headerSize; index++) {
      const el = document.createElement('td');
      el.setAttribute('id', this.getElId(table, this.m_indexRow, index));
      rowEl.appendChild(el);
    }
    table.appendChild(rowEl)
  }

  public popRow = (): boolean => {
    if (this.m_indexRow == 0) {
      return false;
    }
    document.getElementById(this.getRowId(this.m_main, this.m_indexRow))?.remove();
    this.m_indexRow--;
    return true;
  }

  public pushElement = (val: string): void => {
    this.editElement(this.m_main, val, this.m_indexRow, this.m_indexCol);
    let oldIndexCol = this.m_indexCol;
    this.m_indexCol = (this.m_indexCol + 1) % this.m_headerSize;
    if (this.m_indexCol <= oldIndexCol) {
      this.pushRow(this.m_main);
    }
  }
  public ligthPushEl = (val: string): void => {
    this.editElement(this.m_main, val, this.m_indexRow, this.m_indexCol);
  }

  public removeElement = () => {
    let newCol = this.m_indexCol - 1;
    if (newCol == -1) {
      if (!this.popRow())
        return false;
      else
        this.m_indexCol = this.m_headerSize - 1;
    }
    else
      this.m_indexCol = newCol;
  }

  public highlightElement = (indexRow: number, indexCol: number): void => {
    const rows = this.m_main.rows;
    if (indexCol > this.m_headerSize || indexRow > rows.length)
      throw Error("non existent element");
    document.getElementById(this.getElId(this.m_main, indexRow, indexCol))?.setAttribute('class', 'table-el-high');
    this.unHighlightEl(this.m_currHighEl[0], this.m_currHighEl[1]);
    this.m_currHighEl = [indexRow, indexCol];
  }

  protected unHighlightEl = (indexRow: number, indexCol: number): void => {
    const rows = this.m_main.rows;
    if (indexCol > this.m_headerSize || indexRow > rows.length)
      throw Error("non existent element");
    document.getElementById(this.getElId(this.m_main, indexRow, indexCol))?.removeAttribute('class');
  }

  public getCurVal = () => {
    const res = document.getElementById(this.getElId(this.m_main, this.m_indexRow, this.m_indexCol))?.innerHTML;
    if (res === undefined) return "";
    return res;
  }

  protected editElement = (table: HTMLTableElement, val: string, indexRow: number, indexCol: number): void => {
    const rows = table.rows;
    if (indexCol > this.m_headerSize || indexRow > rows.length)
      throw Error("non existent element");
    let curEl = document.getElementById(this.getElId(table, indexRow, indexCol))
    if(curEl === null) return;
    curEl.innerHTML = val;
  }

}

let input = document.getElementById('input') as HTMLTextAreaElement;

class SuggestionList {
  m_main: HTMLDivElement;
  m_size: number;
  m_curHigh: number;
  constructor(list: string[]) {
    this.m_size = 0;
    this.m_curHigh = -1;
    this.m_main = document.createElement('div');
    this.createList(list);
  }

  createList = (list: string[]): HTMLDivElement => {
    this.m_main.setAttribute('id', "suggestionBox");
    this.m_main.setAttribute('class', 'dropdown-content');
    this.m_main.style.maxHeight = "100vh";
    this.m_main.style.overflow = "auto";
    document.getElementById('inputbox')?.appendChild(this.m_main);
    list.forEach(element => {
      this.push(element);
    });
    return this.m_main;
  }

  push = (val: string, indices: number[][] = []) => {
    let listEl = document.createElement('a');
    listEl.style.height = "100%";
    listEl.style.whiteSpace = "normal";
    const string = [...val];
    if (indices.length != 0) {
      let startTag = '<b>';
      let endTag = '</b>';
      const tagLetter = (i: number) => string.splice(i, 1, startTag + string[i] + endTag);
      indices.forEach(element => {
        for (let i = element[0]; i <= element[1]; i++) {
          tagLetter(i);
        }
      });
    };
    listEl.setAttribute('class', 'dropdown-item is-hoverable');
    listEl.innerHTML = string.join('');
    this.m_main.appendChild(listEl);
    this.m_size++;
    const func = () => {
      input.value = listEl.innerText;
      table.ligthPushEl(input.value);
      input.focus();
    }
      ;
    listEl.onclick = func;
  }

  update = (list: string[]) => {
    this.m_main.remove();
    this.createList(list);
  }
  clear = () => {
    this.m_size = 0;
    this.m_curHigh = -1;
    this.m_main.remove();
    this.m_main = document.createElement('div');
    this.createList([]);
  }

  highlight = (index: number) => {
    if (index != -1)
      this.getElFromIndex(index).setAttribute('class', 'dropdown-item is-hoverable is-active is-multiline');
  }
  unHighlight = (index: number) => {
    if (index != -1)
      this.getElFromIndex(index).setAttribute('class', 'dropdown-item is-hoverable is-mulitline');
  }

  highlightNext = () => {
    this.unHighlight(this.m_curHigh)
    ++this.m_curHigh;
    this.m_curHigh = this.m_curHigh % this.m_size;
    this.highlight(this.m_curHigh);
    return this.getElFromIndex(this.m_curHigh).innerText;
  }

  getElFromIndex = (index: number): HTMLDivElement => {
    return this.m_main.children[index] as HTMLDivElement;
  }
}
const resizeInput = (box: HTMLTextAreaElement) => {
  box.style.height = box.scrollHeight.toString() + "px";
  box.style.overflow = "hidden"
};

const ls = new SuggestionList([]);
const table = new Table(['Drug', 'Drug2', 'Drug3']);

const updateLs = () => {
  resizeInput(input);
  if(input.value.length === 0){
    ls.clear();
    table.ligthPushEl(input.value);
    return;
  }
  const update = async () => {
    ls.clear();
    const data = await getSearchResults(input.value, 30);
    if(data === undefined){
      console.log("empty search res");
      return;
    }
    if (typeof (data) === 'undefined') {
      return;
    }
    data.forEach(element => {
      ls.push(element.value, element.indices);
    });
    table.ligthPushEl(input.value);
  }
  update();
}

input.oninput = updateLs;

document.addEventListener('keydown', (event: KeyboardEvent) => {
  if (event.code == 'Tab' || event.code == 'Enter') {
    event.preventDefault();
  }
});

let backonce = false;
input.addEventListener('keydown', (event: KeyboardEvent) => {
  if (event.code == 'Tab') {
    input.value = ls.highlightNext();
    table.ligthPushEl(input.value);
    resizeInput(input);
  }
  else if (event.code == 'Enter') {
    table.pushElement(input.value);
    input.value = '';
    ls.clear();
  }
  else if (input.value == '' && event.code == 'Delete') {
    event.preventDefault();
    table.removeElement();
    input.blur();
    input.value = table.getCurVal();
    input.focus();
  }
});

input.focus();
