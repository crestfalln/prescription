#!/bin/bash

cd api
sh -c "yarn install; yarn build; yarn serve" &

cd ..

cd website
mkdir -p dist
cp src/html/index.html dist/
cp src/css/style.css src/css/disp.css dist/
sh -c "yarn install; yarn build; yarn serve"

