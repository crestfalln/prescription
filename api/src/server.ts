import express from 'express'
import cors from 'cors'
import http from 'http'
import routes from './endpoints'

export default class app {
  private static _instance?: app;
  private context: express.Application;
  private server: http.Server;
  private port: string;

  private constructor() {
    this.context = express();
    this.port = process.env.PORT || '2001';
    routes.use(cors());
    this.context.use(cors());
    this.context.use('/api', routes);
    this.server = this.init(
      async () => {
        console.log(`\x1b[32;1mServer Running on Port\x1b[0m \x1b[34;1m${this.port}\x1b[0m`)
      });
  }
  private init(callback: VoidFunction = () => { }) {
    return this.context.listen(this.port, callback);
  }
  public static get Instance(): app {
    return this._instance || (this._instance = new this());
  }
  public static get Context() {
    return this.Instance.context;
  }
  public static get Port() {
    return this.Instance.port;
  }
  public static restart() {
    if (!this._instance)
      return;
    if (this._instance.server.listening) {
      this._instance.server.close(() => {
        this._instance = undefined
        this.Instance;
      })
    }
  }
  public static quit() {
    if (!this._instance)
      return;
    console.log("Exiting...");
    this.Instance.server.close();
  }
}

