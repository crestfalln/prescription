import { Axios } from 'axios'

const http = new Axios({
  //baseURL: `${window.location.protocol}://${window.location.hostname}:${window.location.port}`
  baseURL: 'http://localhost:2001'
});

interface SearchResult {
  value: string;
  indices: number[][];
}


const getSearchResults = async (val: string, entries: number) => {
  const res = await http.get('/api/search', {
    params: {
      val: val, entries: entries
    }
  });
  if (res.status === 200) {
    return JSON.parse(res.data) as SearchResult[];
  }
}

export { getSearchResults };
