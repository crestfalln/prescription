import Fuse from 'fuse.js'
import list from '../data/drugs'
import { Router, Request, Response } from 'express'

let routes = Router();

interface BadRequest {
  statusCode: number,
  status: string
}

const badRequest: BadRequest = {
  statusCode: 404,
  status: "Resource not found."
};

const fuseOptions = {
  includeScore: true,
  keys: [{ name: 'name' }],
  includeMatches: true
};
const fuse = new Fuse(list, fuseOptions);

routes.get('/search', (req: Request, res: Response) => {
  try {
    if (!(req.query && req.query.val && req.query.entries)) {
      throw badRequest;
    }
    if (!(typeof (req.query.val) === 'string' && typeof (req.query.entries) === 'string')) {
      throw badRequest;
    }
    const entries = parseInt(req.query.entries);
    const query = req.query.val;
    const queryRes = fuse.search(query, { limit: entries });
    let result: { value: string, indices: number[][] }[] = [];
    queryRes.forEach((el) => {
      let indices: number[][] = [];
      el.matches?.at(0)?.indices.forEach(el => {
        indices.push([el[0], el[1]]);
      })
      result.push({ value: el.item.name, indices: indices })
    });
    res.status(200).send(result);
  } catch (err: any) {
    if (!(err.statusCode && err.status)) {
      console.log("Unknown Error");
      res.sendStatus(500);
    }
  }
});

export default routes;
