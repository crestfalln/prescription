const list = [
	{
		"name": "Drug Name",
		"combi": ""
	},
	{
		"name": "Atorvastatin",
		"combi": ""
	},
	{
		"name": "Levothyroxine",
		"combi": ""
	},
	{
		"name": "Lisinopril",
		"combi": ""
	},
	{
		"name": "Metformin Hydrochloride",
		"combi": ""
	},
	{
		"name": "Amlodipine",
		"combi": ""
	},
	{
		"name": "Metoprolol",
		"combi": ""
	},
	{
		"name": "Albuterol",
		"combi": ""
	},
	{
		"name": "Omeprazole",
		"combi": ""
	},
	{
		"name": "Losartan Potassium",
		"combi": ""
	},
	{
		"name": "Simvastatin",
		"combi": ""
	},
	{
		"name": "Gabapentin",
		"combi": ""
	},
	{
		"name": "Acetaminophen; Hydrocodone Bitartrate",
		"combi": ";"
	},
	{
		"name": "Hydrochlorothiazide",
		"combi": ""
	},
	{
		"name": "Sertraline Hydrochloride",
		"combi": ""
	},
	{
		"name": "Montelukast",
		"combi": ""
	},
	{
		"name": "Fluticasone",
		"combi": ""
	},
	{
		"name": "Amoxicillin",
		"combi": ""
	},
	{
		"name": "Furosemide",
		"combi": ""
	},
	{
		"name": "Pantoprazole Sodium",
		"combi": ""
	},
	{
		"name": "Acetaminophen",
		"combi": ""
	},
	{
		"name": "Prednisone",
		"combi": ""
	},
	{
		"name": "Escitalopram Oxalate",
		"combi": ""
	},
	{
		"name": "Fluoxetine Hydrochloride",
		"combi": ""
	},
	{
		"name": "Dextroamphetamine; Dextroamphetamine Saccharate; Amphetamine; Amphetamine Aspartate",
		"combi": ";"
	},
	{
		"name": "Tramadol Hydrochloride",
		"combi": ""
	},
	{
		"name": "Insulin Glargine",
		"combi": ""
	},
	{
		"name": "Bupropion",
		"combi": ""
	},
	{
		"name": "Ibuprofen",
		"combi": ""
	},
	{
		"name": "Rosuvastatin",
		"combi": ""
	},
	{
		"name": "Pravastatin Sodium",
		"combi": ""
	},
	{
		"name": "Trazodone Hydrochloride",
		"combi": ""
	},
	{
		"name": "Tamsulosin Hydrochloride",
		"combi": ""
	},
	{
		"name": "Carvedilol",
		"combi": ""
	},
	{
		"name": "Meloxicam",
		"combi": ""
	},
	{
		"name": "Citalopram",
		"combi": ""
	},
	{
		"name": "Duloxetine",
		"combi": ""
	},
	{
		"name": "Alprazolam",
		"combi": ""
	},
	{
		"name": "Potassium",
		"combi": ""
	},
	{
		"name": "Clopidogrel Bisulfate",
		"combi": ""
	},
	{
		"name": "Aspirin",
		"combi": ""
	},
	{
		"name": "Ranitidine",
		"combi": ""
	},
	{
		"name": "Atenolol",
		"combi": ""
	},
	{
		"name": "Cyclobenzaprine",
		"combi": ""
	},
	{
		"name": "Glipizide",
		"combi": ""
	},
	{
		"name": "Methylphenidate",
		"combi": ""
	},
	{
		"name": "Azithromycin",
		"combi": ""
	},
	{
		"name": "Clonazepam",
		"combi": ""
	},
	{
		"name": "Oxycodone",
		"combi": ""
	},
	{
		"name": "Allopurinol",
		"combi": ""
	},
	{
		"name": "Venlafaxine",
		"combi": ""
	},
	{
		"name": "Hydrochlorothiazide; Lisinopril",
		"combi": ";"
	},
	{
		"name": "Warfarin",
		"combi": ""
	},
	{
		"name": "Propranolol Hydrochloride",
		"combi": ""
	},
	{
		"name": "Hydrochlorothiazide; Losartan Potassium",
		"combi": ";"
	},
	{
		"name": "Cetirizine",
		"combi": ""
	},
	{
		"name": "Estradiol",
		"combi": ""
	},
	{
		"name": "Ethinyl Estradiol; Norethindrone",
		"combi": ";"
	},
	{
		"name": "Lorazepam",
		"combi": ""
	},
	{
		"name": "Quetiapine Fumarate",
		"combi": ""
	},
	{
		"name": "Zolpidem Tartrate",
		"combi": ""
	},
	{
		"name": "Ergocalciferol",
		"combi": ""
	},
	{
		"name": "Budesonide; Formoterol",
		"combi": ";"
	},
	{
		"name": "Spironolactone",
		"combi": ""
	},
	{
		"name": "Ondansetron",
		"combi": ""
	},
	{
		"name": "Insulin Aspart",
		"combi": ""
	},
	{
		"name": "Apixaban",
		"combi": ""
	},
	{
		"name": "Naproxen",
		"combi": ""
	},
	{
		"name": "Lamotrigine",
		"combi": ""
	},
	{
		"name": "Fluticasone Propionate; Salmeterol Xinafoate",
		"combi": ";"
	},
	{
		"name": "Pregabalin",
		"combi": ""
	},
	{
		"name": "Glimepiride",
		"combi": ""
	},
	{
		"name": "Diclofenac",
		"combi": ""
	},
	{
		"name": "Fenofibrate",
		"combi": ""
	},
	{
		"name": "Paroxetine",
		"combi": ""
	},
	{
		"name": "Clonidine",
		"combi": ""
	},
	{
		"name": "Loratadine",
		"combi": ""
	},
	{
		"name": "Diltiazem Hydrochloride",
		"combi": ""
	},
	{
		"name": "Hydroxyzine",
		"combi": ""
	},
	{
		"name": "Amitriptyline",
		"combi": ""
	},
	{
		"name": "Doxycycline",
		"combi": ""
	},
	{
		"name": "Ethinyl Estradiol; Norgestimate",
		"combi": ";"
	},
	{
		"name": "Lisdexamfetamine Dimesylate",
		"combi": ""
	},
	{
		"name": "Sitagliptin Phosphate",
		"combi": ""
	},
	{
		"name": "Latanoprost",
		"combi": ""
	},
	{
		"name": "Cephalexin",
		"combi": ""
	},
	{
		"name": "Tizanidine",
		"combi": ""
	},
	{
		"name": "Finasteride",
		"combi": ""
	},
	{
		"name": "Lovastatin",
		"combi": ""
	},
	{
		"name": "Topiramate",
		"combi": ""
	},
	{
		"name": "Insulin Lispro",
		"combi": ""
	},
	{
		"name": "Sulfamethoxazole; Trimethoprim",
		"combi": ";"
	},
	{
		"name": "Buspirone Hydrochloride",
		"combi": ""
	},
	{
		"name": "Oseltamivir Phosphate",
		"combi": ""
	},
	{
		"name": "Ferrous Sulfate",
		"combi": ""
	},
	{
		"name": "Amoxicillin; Clavulanate Potassium",
		"combi": ";"
	},
	{
		"name": "Valsartan",
		"combi": ""
	},
	{
		"name": "Levetiracetam",
		"combi": ""
	},
	{
		"name": "Hydralazine Hydrochloride",
		"combi": ""
	},
	{
		"name": "Mirtazapine",
		"combi": ""
	},
	{
		"name": "Rivaroxaban",
		"combi": ""
	},
	{
		"name": "Aripiprazole",
		"combi": ""
	},
	{
		"name": "Oxybutynin",
		"combi": ""
	},
	{
		"name": "Esomeprazole",
		"combi": ""
	},
	{
		"name": "Alendronate Sodium",
		"combi": ""
	},
	{
		"name": "Folic Acid",
		"combi": ""
	},
	{
		"name": "Triamcinolone",
		"combi": ""
	},
	{
		"name": "Tiotropium",
		"combi": ""
	},
	{
		"name": "Thyroid",
		"combi": ""
	},
	{
		"name": "Ciprofloxacin",
		"combi": ""
	},
	{
		"name": "Isosorbide Mononitrate",
		"combi": ""
	},
	{
		"name": "Sumatriptan",
		"combi": ""
	},
	{
		"name": "Insulin Detemir",
		"combi": ""
	},
	{
		"name": "Benzonatate",
		"combi": ""
	},
	{
		"name": "Famotidine",
		"combi": ""
	},
	{
		"name": "Diazepam",
		"combi": ""
	},
	{
		"name": "Ropinirole Hydrochloride",
		"combi": ""
	},
	{
		"name": "Hydrochlorothiazide; Triamterene",
		"combi": ";"
	},
	{
		"name": "Benazepril Hydrochloride",
		"combi": ""
	},
	{
		"name": "Metronidazole",
		"combi": ""
	},
	{
		"name": "Methocarbamol",
		"combi": ""
	},
	{
		"name": "Nifedipine",
		"combi": ""
	},
	{
		"name": "Baclofen",
		"combi": ""
	},
	{
		"name": "Methotrexate",
		"combi": ""
	},
	{
		"name": "Testosterone",
		"combi": ""
	},
	{
		"name": "Ezetimibe",
		"combi": ""
	},
	{
		"name": "Celecoxib",
		"combi": ""
	},
	{
		"name": "Guanfacine",
		"combi": ""
	},
	{
		"name": "Donepezil Hydrochloride",
		"combi": ""
	},
	{
		"name": "Hydroxychloroquine",
		"combi": ""
	},
	{
		"name": "Clindamycin",
		"combi": ""
	},
	{
		"name": "Divalproex Sodium",
		"combi": ""
	},
	{
		"name": "Morphine",
		"combi": ""
	},
	{
		"name": "Ethinyl Estradiol; Levonorgestrel",
		"combi": ";"
	},
	{
		"name": "Prednisolone",
		"combi": ""
	},
	{
		"name": "Enalapril Maleate",
		"combi": ""
	},
	{
		"name": "Pioglitazone",
		"combi": ""
	},
	{
		"name": "Cyanocobalamin",
		"combi": ""
	},
	{
		"name": "Norethindrone",
		"combi": ""
	},
	{
		"name": "Meclizine Hydrochloride",
		"combi": ""
	},
	{
		"name": "Valacyclovir",
		"combi": ""
	},
	{
		"name": "Albuterol Sulfate; Ipratropium Bromide",
		"combi": ";"
	},
	{
		"name": "Docusate",
		"combi": ""
	},
	{
		"name": "Liraglutide",
		"combi": ""
	},
	{
		"name": "Hydrocortisone",
		"combi": ""
	},
	{
		"name": "Verapamil Hydrochloride",
		"combi": ""
	},
	{
		"name": "Cefdinir",
		"combi": ""
	},
	{
		"name": "Nortriptyline Hydrochloride",
		"combi": ""
	},
	{
		"name": "Timolol",
		"combi": ""
	},
	{
		"name": "Dulaglutide",
		"combi": ""
	},
	{
		"name": "Promethazine Hydrochloride",
		"combi": ""
	},
	{
		"name": "Acyclovir",
		"combi": ""
	},
	{
		"name": "Fluconazole",
		"combi": ""
	},
	{
		"name": "Methylprednisolone",
		"combi": ""
	},
	{
		"name": "Metformin Hydrochloride; Sitagliptin Phosphate",
		"combi": ";"
	},
	{
		"name": "Ramipril",
		"combi": ""
	},
	{
		"name": "Dexmethylphenidate Hydrochloride",
		"combi": ""
	},
	{
		"name": "Brimonidine Tartrate",
		"combi": ""
	},
	{
		"name": "Oxcarbazepine",
		"combi": ""
	},
	{
		"name": "Risperidone",
		"combi": ""
	},
	{
		"name": "Levofloxacin",
		"combi": ""
	},
	{
		"name": "Chlorthalidone",
		"combi": ""
	},
	{
		"name": "Atomoxetine Hydrochloride",
		"combi": ""
	},
	{
		"name": "Polyethylene Glycol 3350",
		"combi": ""
	},
	{
		"name": "Calcium; Cholecalciferol",
		"combi": ";"
	},
	{
		"name": "Mupirocin",
		"combi": ""
	},
	{
		"name": "Ethinyl Estradiol; Etonogestrel",
		"combi": ";"
	},
	{
		"name": "Drospirenone; Ethinyl Estradiol",
		"combi": ";"
	},
	{
		"name": "Phentermine",
		"combi": ""
	},
	{
		"name": "Carbidopa; Levodopa",
		"combi": ";"
	},
	{
		"name": "Omega-3-acid Ethyl Esters",
		"combi": ""
	},
	{
		"name": "Desogestrel; Ethinyl Estradiol",
		"combi": ";"
	},
	{
		"name": "Guaifenesin",
		"combi": ""
	},
	{
		"name": "Rizatriptan Benzoate",
		"combi": ""
	},
	{
		"name": "Irbesartan",
		"combi": ""
	},
	{
		"name": "Progesterone",
		"combi": ""
	},
	{
		"name": "Doxazosin Mesylate",
		"combi": ""
	},
	{
		"name": "Linagliptin",
		"combi": ""
	},
	{
		"name": "Adalimumab",
		"combi": ""
	},
	{
		"name": "Nitrofurantoin",
		"combi": ""
	},
	{
		"name": "Budesonide",
		"combi": ""
	},
	{
		"name": "Amlodipine Besylate; Benazepril Hydrochloride",
		"combi": ";"
	},
	{
		"name": "Hydrochlorothiazide; Valsartan",
		"combi": ";"
	},
	{
		"name": "Digoxin",
		"combi": ""
	},
	{
		"name": "Acetaminophen; Butalbital",
		"combi": ";"
	},
	{
		"name": "Insulin Degludec",
		"combi": ""
	},
	{
		"name": "Ketoconazole",
		"combi": ""
	},
	{
		"name": "Nitroglycerin",
		"combi": ""
	},
	{
		"name": "Temazepam",
		"combi": ""
	},
	{
		"name": "Amiodarone Hydrochloride",
		"combi": ""
	},
	{
		"name": "Memantine Hydrochloride",
		"combi": ""
	},
	{
		"name": "Canagliflozin",
		"combi": ""
	},
	{
		"name": "Ketorolac Tromethamine",
		"combi": ""
	},
	{
		"name": "Liothyronine Sodium",
		"combi": ""
	},
	{
		"name": "Lithium",
		"combi": ""
	},
	{
		"name": "Dicyclomine Hydrochloride",
		"combi": ""
	},
	{
		"name": "Pramipexole Dihydrochloride",
		"combi": ""
	},
	{
		"name": "Nebivolol Hydrochloride",
		"combi": ""
	},
	{
		"name": "Terazosin",
		"combi": ""
	},
	{
		"name": "Magnesium",
		"combi": ""
	},
	{
		"name": "Colchicine",
		"combi": ""
	},
	{
		"name": "Sucralfate",
		"combi": ""
	},
	{
		"name": "Medroxyprogesterone Acetate",
		"combi": ""
	},
	{
		"name": "Glyburide",
		"combi": ""
	},
	{
		"name": "Carbamazepine",
		"combi": ""
	},
	{
		"name": "Gemfibrozil",
		"combi": ""
	},
	{
		"name": "Nystatin",
		"combi": ""
	},
	{
		"name": "Sildenafil",
		"combi": ""
	},
	{
		"name": "Prazosin Hydrochloride",
		"combi": ""
	},
	{
		"name": "Beclomethasone",
		"combi": ""
	},
	{
		"name": "Linaclotide",
		"combi": ""
	},
	{
		"name": "Desvenlafaxine",
		"combi": ""
	},
	{
		"name": "Insulin Human; Insulin Isophane Human",
		"combi": ";"
	},
	{
		"name": "Clobetasol Propionate",
		"combi": ""
	},
	{
		"name": "Empagliflozin",
		"combi": ""
	},
	{
		"name": "Lansoprazole",
		"combi": ""
	},
	{
		"name": "Erythromycin",
		"combi": ""
	},
	{
		"name": "Guaifenesin; Codeine Phosphate; Pseudoephedrine Hydrochloride",
		"combi": ";"
	},
	{
		"name": "Bumetanide",
		"combi": ""
	},
	{
		"name": "Dexlansoprazole",
		"combi": ""
	},
	{
		"name": "Mometasone",
		"combi": ""
	},
	{
		"name": "Estrogens, Conjugated",
		"combi": ""
	},
	{
		"name": "Hydromorphone Hydrochloride",
		"combi": ""
	},
	{
		"name": "Letrozole",
		"combi": ""
	},
	{
		"name": "Olanzapine",
		"combi": ""
	},
	{
		"name": "Levocetirizine Dihydrochloride",
		"combi": ""
	},
	{
		"name": "Cyclosporine",
		"combi": ""
	},
	{
		"name": "Dapagliflozin",
		"combi": ""
	},
	{
		"name": "Labetalol",
		"combi": ""
	},
	{
		"name": "Anastrozole",
		"combi": ""
	},
	{
		"name": "Mesalamine",
		"combi": ""
	},
	{
		"name": "Sodium",
		"combi": ""
	},
	{
		"name": "Mirabegron",
		"combi": ""
	},
	{
		"name": "Lidocaine",
		"combi": ""
	},
	{
		"name": "Mycophenolate Mofetil",
		"combi": ""
	},
	{
		"name": "Ofloxacin",
		"combi": ""
	},
	{
		"name": "Indomethacin",
		"combi": ""
	},
	{
		"name": "Penicillin V",
		"combi": ""
	},
	{
		"name": "Metoclopramide Hydrochloride",
		"combi": ""
	},
	{
		"name": "Olmesartan Medoxomil",
		"combi": ""
	},
	{
		"name": "Azelastine Hydrochloride",
		"combi": ""
	},
	{
		"name": "Emtricitabine",
		"combi": ""
	},
	{
		"name": "Epinephrine",
		"combi": ""
	},
	{
		"name": "Ipratropium",
		"combi": ""
	},
	{
		"name": "Tamoxifen Citrate",
		"combi": ""
	},
	{
		"name": "Lurasidone Hydrochloride",
		"combi": ""
	},
	{
		"name": "Buprenorphine",
		"combi": ""
	},
	{
		"name": "Calcitriol",
		"combi": ""
	},
	{
		"name": "Ranolazine",
		"combi": ""
	},
	{
		"name": "Dorzolamide Hydrochloride; Timolol Maleate",
		"combi": ";"
	},
	{
		"name": "Formoterol Fumarate; Mometasone Furoate",
		"combi": ";"
	},
	{
		"name": "Betamethasone",
		"combi": ""
	},
	{
		"name": "Calcium",
		"combi": ""
	},
	{
		"name": "Tadalafil",
		"combi": ""
	},
	{
		"name": "Dextroamphetamine Sulfate",
		"combi": ""
	},
	{
		"name": "Methimazole",
		"combi": ""
	},
	{
		"name": "Umeclidinium Bromide; Vilanterol Trifenatate",
		"combi": ";"
	},
	{
		"name": "Umeclidinium Bromide",
		"combi": ""
	},
	{
		"name": "Diphenhydramine Hydrochloride",
		"combi": ""
	},
	{
		"name": "Ticagrelor",
		"combi": ""
	},
	{
		"name": "Fexofenadine Hydrochloride",
		"combi": ""
	},
	{
		"name": "Sotalol Hydrochloride",
		"combi": ""
	},
	{
		"name": "Sodium Fluoride",
		"combi": ""
	},
	{
		"name": "Insulin Isophane",
		"combi": ""
	},
	{
		"name": "Solifenacin Succinate",
		"combi": ""
	},
	{
		"name": "Flecainide Acetate",
		"combi": ""
	},
	{
		"name": "Benztropine Mesylate",
		"combi": ""
	},
	{
		"name": "Eszopiclone",
		"combi": ""
	},
	{
		"name": "Polymyxin B Sulfate; Trimethoprim Sulfate",
		"combi": ";"
	},
	{
		"name": "Phenytoin",
		"combi": ""
	},
	{
		"name": "Bromfenac Sodium; Prednisolone Acetate; Gatifloxacin",
		"combi": ";"
	},
	{
		"name": "Torsemide",
		"combi": ""
	},
	{
		"name": "Sennosides",
		"combi": ""
	},
	{
		"name": "Tolterodine Tartrate",
		"combi": ""
	},
	{
		"name": "Bimatoprost",
		"combi": ""
	},
	{
		"name": "Etanercept",
		"combi": ""
	},
	{
		"name": "Travoprost",
		"combi": ""
	},
	{
		"name": "Minocycline Hydrochloride",
		"combi": ""
	},
	{
		"name": "Bisoprolol Fumarate; Hydrochlorothiazide",
		"combi": ";"
	},
	{
		"name": "Nabumetone",
		"combi": ""
	},
	{
		"name": "Isotretinoin",
		"combi": ""
	},
	{
		"name": "Doxepin Hydrochloride",
		"combi": ""
	},
	{
		"name": "Primidone",
		"combi": ""
	},
	{
		"name": "Dexamethasone",
		"combi": ""
	},
	{
		"name": "Bisoprolol Fumarate",
		"combi": ""
	},
	{
		"name": "Exenatide",
		"combi": ""
	},
	{
		"name": "Chlorhexidine",
		"combi": ""
	},
	{
		"name": "Ethinyl Estradiol; Norgestrel",
		"combi": ";"
	},
	{
		"name": "Dutasteride",
		"combi": ""
	},
	{
		"name": "Modafinil",
		"combi": ""
	},
	{
		"name": "Olopatadine",
		"combi": ""
	},
	{
		"name": "Fentanyl",
		"combi": ""
	},
	{
		"name": "Telmisartan",
		"combi": ""
	},
	{
		"name": "Polyethylene Glycol 3350 With Electrolytes",
		"combi": ""
	},
	{
		"name": "Tretinoin",
		"combi": ""
	},
	{
		"name": "Dexamethasone; Neomycin Sulfate; Polymyxin B Sulfate",
		"combi": ";"
	},
	{
		"name": "Pseudoephedrine",
		"combi": ""
	},
	{
		"name": "Insulin Human",
		"combi": ""
	},
	{
		"name": "Sacubitril; Valsartan",
		"combi": ";"
	},
	{
		"name": "Pancrelipase Lipase; Pancrelipase Protease; Pancrelipase Amylase",
		"combi": ";"
	},
	{
		"name": "Brompheniramine Maleate; Dextromethorphan Hydrobromide; Pseudoephedrine Hydrochloride",
		"combi": ";"
	},
	{
		"name": "Amlodipine+Metaplol",
		"combi": ";"
	},
	{
		"name": "Glimepiride+Metformin",
		"combi": ";"
	}
];

export default list;
