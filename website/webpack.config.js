const path = require('path')

module.exports = {
  target: 'web',
  mode: 'production',
  entry: './src/js/index.ts', // make sure this matches the main root of your code
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  devServer: {
    static: './dist'
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },

  output: {
    path: path.join(__dirname, 'dist/'), // this can be any path and directory you want
    filename: 'bundle.js',
  },
}
